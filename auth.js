const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';

// Token Creation
module.exports.createAccessToken = (user) => {

/*	{
    "firstName": "John",
    "lastName": "Smith",
    "email": "john@mail.com",
    "password": "$2b$10$MRIdoucI6UGQV8jz3l/p2.628sTQ8KJ1h1YsoADmR6190wm0.cX0S",
    "isAdmin": false,
    "mobileNo": "09123456789",
    "_id": "63dc65b31b1219f1973cb0a1",
    "enrollments": [],
    "__v": 0
}*/
	// When the user log in, a token will be created with user's information
	// Payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// jwt.sign(data/payload, secretkey, options) method 
	return jwt.sign(data, secret, {})
}

// Token Verification

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;
	// Token is ID access token

	if(typeof token !== 'undefined') {
		console.log(token)

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {

			if(error) {
				return res.send({auth: 'failed'})
			} else {
				next();
			}
		})
	} 
	else {
		return res.send({auth: 'failed'})
	}
}

// Token Decryption
module.exports.decode = (token) => {

	if(typeof token !== 'undefined') {
		token = token.slice(7, token.length)
// req.headers[TOKEN] cutting the "bearer with space"
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZGM2NWIzMWIxMjE5ZjE5NzNjYjBhMSIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NzU0MDUxOTF9.GRnD_ZEyGWW__zO-1gB6s6cCT2QiQfKu64qL9ln5Trc


		return jwt.verify(token, secret, (error, data) => {
			
			if(error) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}

		})
	} else {
		return null
	}
}