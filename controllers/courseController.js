const Course = require('../models/Course')

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})

}

/*module.exports.addCourse = (reqBody) => {

	if(data.isAdmin) {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	});

	// To print if the log in is not an Admin

	let message = Promise.resolve('User must be ADMIN to access this.')
	return message.then((value) => {
	return {value}
	})

	};
};*/

//////////////// SESSION 40

// Retrive all course
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// MINI ACTIVITY: Retrieve all the active course

module.exports.getActiveCourses = () => {

		return Course.find({isActive: true}).then(result => {
		return result;
	})

}

module.exports.getCourse = (reqParams) => {

		return Course.findById(reqParams.courseId).then(result => {
		return result;
	})

}

module.exports.updateCourse = (reqParams, reqBody) => {

	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

		/*Syntax:
		findByIdandUpdate(document ID, updatetobeapplied) */

		return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})

}

// ACTIVITY S40 PATCH

module.exports.archiveCourse = (reqParams, reqBody) => {

	let archiveCourse = {
		isActive: reqBody.isActive
	}

		/*Syntax:
		findByIdandUpdate(document ID, updatetobeapplied) */

		return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})

}

