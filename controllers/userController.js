const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth')



// Checking if the email exists in the database.
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {

		if(result.length > 0) {
			return true

		// no duplicate email found // User is not yet registered in our database
		} else {
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// bcrypt.hashSync(<dataToBeHash>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return user
		}
	});
};

// LOG IN USING USERNAME AND PASSWORD

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false
		} else {

			// compareSync(dataFromRequestBody, encryptedDataFromDatabase) 
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}


////////////////////////////// ACTIVITY

// POST using ID to search for a specific account.

module.exports.getProfile = (reqBody) => {
	return User.findOne({_id:reqBody.userId}).then(result => {

		if(result == null) {
			return false
		}
			result.password=""
			return  result
		
	})
}

// Retreive user details (For Frontend)
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};


//////////////////// SESSION 40

// Enrolle user to a course

module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the courseId in the user's enrollments array
		user.enrollments.push({courseId : data.courseId});
		return user.save().then((user, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(
		course => {
			// Adds the userId in the course's enrollees array
			course.enrollees.push({userId : data.userId});
			return course.save().then((course, error) => {
				if (error) {
					return false;
				} else {
					return true;
				};
			});
		});
		// Condition that will check if the user and course documents have been updated
		if(isUserUpdated && isCourseUpdated){
			// enrollment successful
			return true;
		} else {
			// failed enrollment
			return false;
		};
};
