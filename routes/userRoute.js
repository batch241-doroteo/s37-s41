const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController')
const auth = require('../auth')

// Route for checking if the user's email already exists in our database
router.post('/checkEmail', (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for User Registration
router.post('/register', (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for User Authentication

router.post('/login', (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})



//////////////////////////////// ACTIVITY

router.post('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
})

// Retreive user details (For Frontend)
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


////////////////////////// SESSION 41

// WITHOUT OUT LOG IN

// Route to entoll user to a course
// router.post("/enroll", (req, res) => {
// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	};
// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// });

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	};
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});







module.exports = router;