const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController')
const auth = require('../auth')

// Route for creating a course for ADMIN AUTHORIZATION
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == false) {
		res.send(false)
	} else {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}
	

})

//EASIER RESOLUTION FOR GIVING AUTHORIZATION TO ADMIN

/*router.get('/', (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourses(req.body).then(resultFromController => res.send(resultFromController))
})*/




/*
router.post('/', auth.verify, (req, res) => {
	const data = { course: reqBody,
	isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	

})
*/

////////////////////// SESSION 40

// Route for retrieving ALL the courses
router.get('/all', (req, res) => {
	courseController.getAllCourses(req.body).then(resultFromController => res.send(resultFromController))
})

// MINI ACTIVITY: Retrieve ALL the ACTIVE course

router.get('/activeCourse', (req, res) => {
	courseController.getActiveCourses(req.body).then(resultFromController => res.send(resultFromController))
})

// Retrieving specific ID course

router.get('/:courseId', (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Updating a course

router.put('/:courseId', auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body ).then(resultFromController => res.send(resultFromController))
})

// S40 ACTIVITY PATCH

router.patch('/:courseId/archive', auth.verify, (req, res) => {

	courseController.archiveCourse(req.params, req.body ).then(resultFromController => res.send(resultFromController))
})



module.exports = router;



